//
//  DBManager.swift
//  Inventario Medicinal
//
//  Created by Edward  Becerra on 4/27/20.
//  Copyright © 2020 ContruSW. All rights reserved.
//

import Foundation

class DBManager {
    private var drugstoreRecords: [Drugstore]
    private static var shared: DBManager?
    private let factory: DSFactory
    
    private init() {
        factory = DSFactory()
        drugstoreRecords = []
    }
    
    public static func getInstance() -> DBManager {
        if shared == nil {
            shared = DBManager()
            shared?.loadData()
            print(shared?.drugstoreRecords.count ?? "error")
        }
        return shared!
    }
    
    public func getDrugstoreRecords() -> [Drugstore] {
        return self.drugstoreRecords
    }
    
    private func loadData() {
        var data: Data
        let files = ["Ahumada.json","CruzVerde.json","Salcobrand.json"]
        
        for filename in files {
            guard let file = Bundle.main.url(forResource: filename, withExtension: nil) else {
                fatalError("Couldn't find \(filename) in main bundle.")
            }
            do{
                data = try Data(contentsOf: file)
            } catch {
               fatalError("Couldn't load \(filename) from main bundle:\n \(error)")
            }
            let records = factory.createMultipleDrugstore(data: data)
            self.drugstoreRecords.append(contentsOf: records)
        }
    }

}
