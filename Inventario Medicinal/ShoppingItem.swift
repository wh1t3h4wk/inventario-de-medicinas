//
//  ShoppingItem.swift
//  Inventario Medicinal
//
//  Created by Edward  Becerra on 4/28/20.
//  Copyright © 2020 ContruSW. All rights reserved.
//

import Foundation

struct ShoppingItem {
    let supplyName: String
    let drugstoreName: String
    let address: String
    let phoneNumber: String
    let price: String
    
    init(supplyName: String, drugstoreName: String, address: String, phoneNumber: String, price: String) {
        self.supplyName = supplyName
        self.drugstoreName = drugstoreName
        self.address = address
        self.phoneNumber = phoneNumber
        self.price = price
    }
    
}
