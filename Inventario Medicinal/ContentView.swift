//
//  ContentView.swift
//  Inventario Medicinal
//
//  Created by Edward  Becerra on 4/27/20.
//  Copyright © 2020 ContruSW. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            ScrollView(showsIndicators: false) {
                NavigationLink(destination: SearchView()){
                   CardView(headline: "Search",title: "Buscar",icon: "magnifyingglass",color: UIColor.systemIndigo)
                }
                                   
                NavigationLink(destination: ShoppingCardView()){
                   CardView(headline: "Shopping List",title: "Lista de Compras",icon: "cart",color: UIColor.systemGreen)
                }
            }
            .navigationBarTitle(Text("Inventario Medicinal"))
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
