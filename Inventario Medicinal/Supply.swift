//
//  Supply.swift
//  Inventario Medicinal
//
//  Created by Edward  Becerra on 4/27/20.
//  Copyright © 2020 ContruSW. All rights reserved.
//

import Foundation

struct Supply: Decodable {
    private var name: String
    private var category: String
    private var format: String
    private var price: String
    
    func getName() -> String {
        return self.name
    }
    
    func getCategory() -> String{
        return self.category
    }
    
    func getFormat() -> String {
        return self.format
    }
    
    func getPrice() -> String {
        return self.price
    }
}
