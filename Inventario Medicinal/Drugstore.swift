//
//  Drugstore.swift
//  Inventario Medicinal
//
//  Created by Edward  Becerra on 4/27/20.
//  Copyright © 2020 ContruSW. All rights reserved.
//

import Foundation

protocol Drugstore {
    func getName() -> String
    func getAddress() -> String
    func getPhoneNumber() -> String
    func getLogo() -> String
    func retrieveSupplies() -> [Supply]
}
