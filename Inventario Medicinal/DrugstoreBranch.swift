//
//  DrugstoreBranch.swift
//  Inventario Medicinal
//
//  Created by Edward  Becerra on 4/27/20.
//  Copyright © 2020 ContruSW. All rights reserved.
//

import Foundation

struct DrugstoreBranch: Drugstore, Decodable {
    var name: String
    var address: String
    var phoneNumber: String
    var logo: String
    var supplies: [Supply]
    
    func getName() -> String {
        return self.name
    }
    
    func getAddress() -> String {
        return self.address
    }
    func getPhoneNumber() -> String {
        return self.phoneNumber
    }
    
    func getLogo() -> String {
        return self.logo
    }
    
    func retrieveSupplies() -> [Supply] {
        return self.supplies
    }
}
