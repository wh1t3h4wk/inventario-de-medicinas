//
//  DSFactoryInterface.swift
//  Inventario Medicinal
//
//  Created by Edward  Becerra on 4/27/20.
//  Copyright © 2020 ContruSW. All rights reserved.
//

import Foundation

protocol DSFactoryInterface {
    func createDrugstore(data: Data) -> Drugstore
    func createMultipleDrugstore(data: Data) -> [Drugstore]
}
