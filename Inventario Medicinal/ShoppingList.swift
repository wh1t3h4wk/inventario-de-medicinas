//
//  ShoppingList.swift
//  Inventario Medicinal
//
//  Created by Edward  Becerra on 4/28/20.
//  Copyright © 2020 ContruSW. All rights reserved.
//

import Foundation

struct ShoppingList {
    private var shoppingList: [ShoppingItem]
    private static var shared: ShoppingList?
    
    private init () {
        shoppingList = []
    }
    
    public static func getInstance() -> ShoppingList {
        if shared == nil {
            shared = ShoppingList()
        }
        return shared!
    }
    
    mutating func addItem(supplyName: String, drugstoreName: String, address: String, phoneNumber: String, price: String) {
        let item = ShoppingItem(supplyName: supplyName, drugstoreName: drugstoreName, address: address, phoneNumber: phoneNumber, price: price)
        shoppingList.append(item)
    }
    
    mutating func removeItems() {
        shoppingList.removeAll()
    }
    
    func exportPDF() {
        print("Not Implemented yet")
    }
}
