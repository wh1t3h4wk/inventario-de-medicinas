//
//  DSFactory.swift
//  Inventario Medicinal
//
//  Created by Edward  Becerra on 4/27/20.
//  Copyright © 2020 ContruSW. All rights reserved.
//

import Foundation

class DSFactory: DSFactoryInterface {
    func createMultipleDrugstore(data: Data) -> [Drugstore] {
        do{
            let decoder = JSONDecoder()
            let decoded = try decoder.decode([DrugstoreBranch].self, from: data)
            return decoded
        }catch {
            fatalError("Couldn't decode data:\n \(error)")
        }
    }
    
    func createDrugstore(data: Data) -> Drugstore {
        do{
            let decoder = JSONDecoder()
            let decoded = try decoder.decode(DrugstoreBranch.self, from: data)
            return decoded
        }catch {
            fatalError("Couldn't decode data:\n \(error)")
        }
    }
}
