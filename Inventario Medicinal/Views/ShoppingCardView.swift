//
//  ShoppingCardView.swift
//  Inventario Medicinal
//
//  Created by Edward  Becerra on 4/28/20.
//  Copyright © 2020 ContruSW. All rights reserved.
//

import SwiftUI

struct ShoppingCardView: View {
    var body: some View {
        Text(/*@START_MENU_TOKEN@*/"Hello, World!"/*@END_MENU_TOKEN@*/)
    }
}

struct ShoppingCardView_Previews: PreviewProvider {
    static var previews: some View {
        ShoppingCardView()
    }
}
