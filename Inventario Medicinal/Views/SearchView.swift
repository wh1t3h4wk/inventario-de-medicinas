//
//  SearchView.swift
//  Inventario Medicinal
//
//  Created by Edward  Becerra on 4/28/20.
//  Copyright © 2020 ContruSW. All rights reserved.
//

import SwiftUI

struct SearchView: View {
    @State private var showCancelButton: Bool = false
    @State private var searchText = ""
    
    let records = DBManager.getInstance().getDrugstoreRecords() as! [DrugstoreBranch]
    
    var body: some View {
        VStack{
            HStack {
                HStack {
                    Image(systemName: "magnifyingglass")

                    TextField("Search", text: $searchText, onEditingChanged: { isEditing in
                        self.showCancelButton = true
                    }, onCommit: {
                        print("onCommit")
                    }).foregroundColor(.primary)

                    Button(action: {
                        self.searchText = ""
                    }) {
                        Image(systemName: "xmark.circle.fill").opacity(searchText == "" ? 0 : 1)
                    }
                }
                .padding(EdgeInsets(top: 8, leading: 6, bottom: 8, trailing: 6))
                .foregroundColor(.secondary)
                .background(Color(.secondarySystemBackground))
                .cornerRadius(10.0)

                if showCancelButton  {
                    Button("Cancel") {
                            UIApplication.shared.endEditing(true)
                            self.searchText = ""
                            self.showCancelButton = false
                    }
                    .foregroundColor(Color(.systemBlue))
                }
            }
            .padding(.horizontal)
            .navigationBarHidden(showCancelButton)
            
            //Aqui pondria mi lista (si tan solo pudiera crearla...)
            EmptyView()
        }
    }
}

struct SearchView_Previews: PreviewProvider {
    static var previews: some View {
        SearchView()
    }
}
