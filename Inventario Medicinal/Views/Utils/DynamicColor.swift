//
//  DynamicColor.swift
//  Himnario
//
//  Created by Edward  Becerra on 1/29/20.
//  Copyright © 2020 MonkeyDevCo. All rights reserved.
//

import Foundation
import SwiftUI

class DynamicColor {
    static func dynamicColor (dark: UIColor, light: UIColor) -> UIColor {
        return UIColor { (traitCollection: UITraitCollection) -> UIColor in
            switch traitCollection.userInterfaceStyle {
            case
              .unspecified,
              .light: return light
            case .dark: return dark
            @unknown default:
                fatalError("Dynamic color error");
            }
        }
    }
}
