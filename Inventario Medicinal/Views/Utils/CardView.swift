//
//  CardView.swift
//  Himnario
//
//  Created by Edward  Becerra on 1/25/20.
//  Copyright © 2020 MonkeyDevCo. All rights reserved.
//375.0

import SwiftUI

struct CardView: View {
    let headline: String
    let title: String
    let icon: String
    let color: UIColor
    
    let colorLight = UIColor(displayP3Red: 0.98, green: 0.98, blue: 0.98, alpha: 1.0)
    
    var body: some View {
        ZStack{
            RoundedRectangle(cornerRadius: 15, style: .continuous)
            .fill(Color(DynamicColor.dynamicColor(dark: .darkGray, light: colorLight)))
                .frame(maxWidth:.infinity, minHeight: 150, maxHeight: 150, alignment: .center)
            .shadow(color: Color(UIColor.systemGray3), radius: 3, x: 5, y: 10)
            HStack{
                VStack(alignment: .leading){
                    Text(headline)
                        .font(.headline)
                        .foregroundColor(Color(color))
                    Text(title)
                        .font(.title)
                        .foregroundColor(.primary)
                }
                
                Spacer()
                
                ZStack {
                    RoundedRectangle(cornerRadius: 15, style: .continuous)
                    .fill(Color(color))
                    .frame(width: 80, height: 80)
                    
                    Image(systemName: icon)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                        .frame(width: 40, height: 32)
                        .foregroundColor(Color.white)
                }
                .padding()
            }
            .frame(maxWidth:(.infinity), minHeight: 150, maxHeight: 150, alignment: .center)
            .padding()
        }
            .padding(.leading)
            .padding(.trailing)
    }
}

struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        CardView(headline: "Feedback",title: "Comentarios",icon: "person.2.fill",color: UIColor.systemYellow)
    }
}
